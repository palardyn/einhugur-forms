Note to run the test project you may need to right click EinhugurFormsTest inside the solution and click "Set as startup project"


Current documentation can be downloaded from:

https://einhugur.net/Downloads/EinhugurForms/Documentation.zip

Note the documentation is Auto generated, and currently has Xojo dialect (until we have added C# dialect in our documentation generator)