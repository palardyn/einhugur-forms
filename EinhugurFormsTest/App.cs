﻿using System;
using AppKit;
using Foundation;
using Einhugur.Forms;
using System.Collections.Generic;
using Einhugur.Arguments;
using Einhugur.Menus;
using Einhugur.Dialogs;
using Einhugur;

namespace TestApp
{
    [Register("AppDelegate")]
    public class App : Application
    {
        protected override void Opening()
        {
            NSApplication.SharedApplication.MainMenu = new AppMenu("Test");

            // Menu Routing is supported !
            // Rules are:
            // 1. If Window wants it then Window gets it
            // 2. If Window did not want it then Application gets it if it wants it.
            // 3. Menu item gets it if noone has it
            MenuHandlers = new Dictionary<MenuItem, EventHandler<MenuEventArgs>>()
            {
                { AppMenu.QuitMenuItem,  QuitMenuItem_Action}
            };
        }


        protected override void Opened()
        {
            var window = new Window1();

            window.Show();
        }

        protected override bool OpenFile(FileSystemItem file)
        {
            MessageBox.Show($"Wants to open file: {file.FullPath}");
            return true;
        }

        void QuitMenuItem_Action(object sender, EventArgs e)
        {
            Quit();
        }

       
        static void Main(string[] args)
        {
            Application.Init(typeof(App), args);    
        }
    }
}

