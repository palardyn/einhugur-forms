﻿using System;
using System.Collections.Generic;
using Einhugur.Forms;
using static Einhugur.Forms.Window;

namespace TestApp
{
    public partial class Window3 : Window
    {
        ListBox listBox1;
        Label label1;
        Button button1;
        Scrollbar scrollbar1;
        Scrollbar scrollbar2;
        Label label2;

        protected override void SetupWindow(WindowCreationParameters prm)
        {
            prm.Left = 400;
            prm.Top = 150;
            prm.Width = 400;
            prm.Height = 500;
            prm.Title = "My third window";

        }

        protected override IEnumerable<Control> SetupControls()
        {
            listBox1 = new ListBox(10, 10, 380, 220, new string[] { "Test", "xxx" })
            {
                LockLeft = true,
                LockRight = true,
                LockTop = true,
                LockBottom = true,
                Columns = 3,
                Multiselection = true,
                AlternatingRowolors = true
            };
            listBox1.SelectionChanged += ListBox1_SelectionChanged;

            label1 = new Label(10, 240, 380, 22)
            {
                LockLeft = true,
                LockRight = true,
                LockBottom = true
            };

            button1 = new Button(10, 270, 110, 22)
            {
                Caption = "Get selection",
                LockLeft = true,
                LockBottom = true
            };
            button1.Pressed += Button1_Pressed;

            scrollbar1 = new Scrollbar(30, 300, 15, 150);
            scrollbar1.ValueChanged += Scrollbar1OnValueChanged;
            
            scrollbar2 = new Scrollbar(50, 450, 150, 15);
            scrollbar2.ValueChanged += Scrollbar2OnValueChanged;
                
            label2 = new Label(60, 350, 300, 30);

            return new Control[]
            {
                listBox1,
                label1,
                button1,
                scrollbar1,
                scrollbar2,
                label2
            };
        }

        
    }
}

