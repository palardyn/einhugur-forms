﻿using System;
using AppKit;
using System.Collections.Generic;
using Einhugur;
using Einhugur.Forms;
using Einhugur.Arguments;
using Einhugur.Drawing;
using Einhugur.Menus;
using Einhugur.Threading;

namespace TestApp
{
    public partial class Window1
    {
        private Button button1;
        private Button button2;
        private TextField textField1;
        private Label label1;
        private Label label2;
        private TextField textField2;
        private Canvas canvas1;
        private Button button3;
        private Button button4;
        private Button button5;
        private Button button6;
        private Button button8;
        private Button button7;
        private Checkbox checkbox1;
        private RadioButton radioButton1;
        private RadioButton radioButton2;
        private PopupButton popupButton1;
        private Timer timer1;
        private TextField textField3;
        private ProgressBar progressBar;

        protected override void SetupWindow(WindowCreationParameters prm)
        {
            prm.Left = 200;
            prm.Top = 100;
            prm.Width = 520;
            prm.Height = 400;
            prm.Title = "My first window";
            prm.MinHeight = 200;
            prm.InitialPlacement = WindowPlacement.Center;
            prm.MenuHandlers = new Dictionary<MenuItem, EventHandler<MenuEventArgs>>()
            {
                { AppMenu.QuitMenuItem, QuitMenuItem_Click }
            };
        }

        protected override IEnumerable<Control> SetupControls()
        {
            button4 = new Button(30, 20, 112, 30)
            {
                Caption = "Show window 2"
            };
            button4.Pressed += Button4_Pressed;

            button5 = new Button(130, 20, 112, 30)
            {
                Caption = "Show window 3"
            };
            button5.Pressed += Button5_Pressed;
            
            button6 = new Button(270, 20, 120, 23)
            {
                Caption = "Question dialog"
            };
            button6.Pressed += Button6_Pressed;
            
            button8 = new Button(390, 20, 120, 23)
            {
                Caption = "Show window 4"
            };
            button8.Pressed += Button8_Pressed;

            button1 = new Button(30, 50, 120, 23)
            {
                Caption = "Open svg file ..."
            };
            button1.Pressed += Button1_Pressed;

            button2 = new Button(150, 50, 120, 23)
            { 
                Caption = Localized.Text("YesButton", "Some fallback text"),
                LockLeft = true,
                LockRight = true
            };
            button2.Pressed += Button2_Pressed;

            button7 = new Button(270, 50, 120, 23)
            {
                Caption = "Show URL"
            };
            button7.Pressed += Button7_Pressed;

            progressBar = new ProgressBar(270, 85, 150, 20)
            {
                MinValue = 0,
                MaxValue = 10,
                Value = 5,
                Intermediate = true,
                DisplayedWhenStopped = false
            };

            checkbox1 = new Checkbox(30, 80, 200, 22)
            {
                Caption = "Some checkbox",
                Value = CheckBoxState.Checked
            };
            checkbox1.Action += Checkbox1_Action;

            radioButton1 = new RadioButton(30, 110, 200, 22)
            {
                Caption = "Option 1",
                Value = false
            };
            radioButton1.Action += RadioButton1_Action;

            radioButton2 = new RadioButton(30, 135, 200, 22)
            {
                Caption = "Option 2",
                Value = true
            };
            radioButton2.Action += RadioButton2_Action;

            popupButton1 = new PopupButton(300, 135, 100, 22)
            {
                Items = new[]
                {
                    "Item 1",
                    "Item 2",
                    "Item 3",
                    "Item 4"
                },
                SelectedIndex = 2,
                ToolTip = "Test"
            };
            popupButton1.SelectionChanged += PopupButton1_SelectionChanged;


            label2 = new Label(5, 202, 90, 22)
            {
                Caption = "Name:",
                Alignment = TextAlignment.Right
            };

            textField1 = new TextField(100, 200, 300, 22)
            {
                LockLeft = true,
                LockRight = true,
                Placeholder = "Some placeholder",
                Tooltip = "Some tooltip",
                RoundedFrame = true,
            };
            textField1.GotFocus += TextField1_GotFocus;

            label1 = new Label(5, 232, 90, 22)
            {
                Caption = "Address:",
                Alignment = TextAlignment.Right
            };

            textField2 = new TextField(100, 230, 300, 22)
            {
                LockLeft = true,
                LockRight = true
            };

            canvas1 = new Canvas(10, 270, 400, 100, () =>
            {
                button3 = new Button(30, 20, 190, 22)
                {
                    Caption = "Button on Canvas",
                    LockLeft = true,
                    LockRight = true
                };
                button3.Pressed += Button3_Pressed;

                textField3 = new TextField(100, 50, 300, 22)
                {
                    LockLeft = true,
                    LockRight = true
                };
                

                return new Control[] { button3 , textField3 };
            })
            {
                ShowFocusRing = true,
                AcceptsFocus = false,
            };
            

            canvas1.Paint += Canvas1_Paint;
            canvas1.MouseDown += Canvas1_MouseDown;

            timer1 = new Timer()
            {
                Interval = 2.0
            };
            
            timer1.Action += Timer1_Action;

            return new Control[]
            {
                button5,
                button6,
                button8,
                button4,
                button1,
                button2,
                button7,
                progressBar,
                checkbox1,
                radioButton1,
                radioButton2,
                popupButton1,
                label2,
                textField1,
                label1,
                textField2,
                canvas1
            };
        }

        
    }
}

