﻿using System;
using System.Collections.Generic;
using Einhugur.Forms;

namespace TestApp
{
    public partial class Window2
    {
        GroupBox myGroupBox;
        RadioButton radioButton1;
        RadioButton radioButton2;
        GroupBox myGroupBox2;
        RadioButton radioButton3;
        RadioButton radioButton4;
        TabControl tabControl1;
        TabControl.Tab tab1;
        TabControl.Tab tab2;
        Button button1;
        Button button2;

        protected override void SetupWindow(WindowCreationParameters prm)
        {
            prm.Left = 400;
            prm.Top = 150;
            prm.Width = 400;
            prm.Height = 500;
            prm.Title = "My second window";

        }

        protected override IEnumerable<Control> SetupControls()
        {
            myGroupBox = new GroupBox(10, 10, 380, 220, () =>
            {
                radioButton1 = new RadioButton(30, 50, 200, 22)
                {
                    Caption = "Option 1",
                    Value = false
                };
                radioButton1.Action += RadioButton1_Action;

                radioButton2 = new RadioButton(30, 75, 200, 22)
                {
                    Caption = "Option 2",
                    Value = true
                };
                radioButton2.Action += RadioButton2_Action;

                tabControl1 = new TabControl(0, 90, 365, 100, () =>
                {
                    tab1 = new TabControl.Tab(() =>
                    {
                        button1 = new Button(20, 0, 100, 23)
                        {
                            Caption = "Some button"
                        };

                        return new Control[] { button1 };
                    })
                    {
                        Caption = "Tab 1"
                    };

                    tab2 = new TabControl.Tab(() =>
                    {
                        button2 = new Button(100, 30, 150, 23)
                        {
                            Caption = "Some other button"
                        };

                        return new Control[] { button2 };
                    })
                    {
                        Caption = "Tab 2"
                    };

                    return new TabControl.Tab[] { tab1, tab2 };
                });
                tabControl1.TabSelected += TabControl1_TabSelected;

                return new Control[] { radioButton1, radioButton2, tabControl1 };
            })
            {
                Caption = "My group box",
                LockLeft = true,
                LockRight = true,
                LockTop = true,
            };

            myGroupBox2 = new GroupBox(10, 230, 380, 260, () =>
            {
                radioButton3 = new RadioButton(30, 110, 200, 22)
                {
                    Caption = "Option 1",
                    Value = false
                };
                radioButton3.Action += RadioButton3_Action;

                radioButton4 = new RadioButton(30, 135, 200, 22)
                {
                    Caption = "Option 2",
                    Value = true
                };
                radioButton4.Action += RadioButton4_Action;

                return new Control[] { radioButton3, radioButton4 };
            })
            {
                Caption = "My group box 2",
                LockLeft = true,
                LockRight = true,
                LockTop = true,
                LockBottom = true
            };




            return new Control[]
            {
                myGroupBox,
                myGroupBox2
            };
        }

        
    }
}

