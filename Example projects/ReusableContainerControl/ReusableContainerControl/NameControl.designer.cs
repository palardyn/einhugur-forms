using System.Collections.Generic;
using Einhugur.Forms;

namespace ReusableContainerControl
{
    public partial class NameControl : Canvas
    {
        private TextField txFirstName;
        private TextField txMiddleName;
        private TextField txLastName;
        
        public NameControl(float x, float y, float width, float height)
            : base(x, y, width, height, null)
        {
            // Note when using Canvas as base for Contrainer control
            // which the canvas is very suited for, then we put null in the setupControls above
            // and instead manuall call setup controls like bellow:
            this.SetupControls(SetupPageControls);
        }
        
        private IEnumerable<Control> SetupPageControls()
        {
            txFirstName = new TextField(1, 1, 100, 22)
            {
                LockTop = true,
                LockLeft = true, 
                Placeholder = "First"
            };
            txFirstName.TextChanged += txFirstName_TextChanged;
            
            txMiddleName = new TextField(105, 1, 60, 22)
            {
                LockTop = true,
                LockLeft = true,
                Placeholder = "Middle"
            };
            txMiddleName.TextChanged += txMiddleName_TextChanged;
            
            txLastName = new TextField(170, 1, 100, 22)
            {
                LockTop = true,
                LockLeft = true,
                Placeholder = "Last"
            };
            txLastName.TextChanged += txLastName_TextChanged;
            

            return new Control[]
            {
                txFirstName,
                txMiddleName,
                txLastName
            };
        }
    }
}