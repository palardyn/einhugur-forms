using System;

namespace ReusableContainerControl
{
    public partial class NameControl
    {
        public event EventHandler NameChanged;

        public string FullName => txFirstName.Text + (txFirstName.Text.Length > 0 ? " " : "") +
                                  txMiddleName.Text + (txMiddleName.Text.Length > 0 ? " " : "") +
                                  txLastName.Text + (txLastName.Text.Length > 0 ? " " : "");

        public string FirstName
        {
            get => txFirstName.Text;
            set => txFirstName.Text = value;
        }

        public string MiddleName
        {
            get => txMiddleName.Text;
            set => txMiddleName.Text = value;
        }
        
        public string LastName
        {
            get => txLastName.Text;
            set => txLastName.Text = value;
        }

        // We override SetFocus in case if someone calls SetFocus on our control then
        // we want the focus to go on the first logical focusable part in the control.
        public override void SetFocus()
        {
            txFirstName.SetFocus();
        }

        // It is good practice to fire the event with a protected method so
        // that the event control can be used for derived classes.
        protected void OnNameChanged()
        {
            NameChanged?.Invoke(this, EventArgs.Empty);
        }

        private void txFirstName_TextChanged(object sender, EventArgs args)
        {
            OnNameChanged();
        }
        
        private void txMiddleName_TextChanged(object sender, EventArgs args)
        {
            OnNameChanged();
        }
        
        private void txLastName_TextChanged(object sender, EventArgs args)
        {
            OnNameChanged();
        }
    }
}