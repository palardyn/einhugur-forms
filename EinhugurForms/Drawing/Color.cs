﻿using System;
using System.Diagnostics.CodeAnalysis;
using AppKit;
using CoreGraphics;

namespace Einhugur.Drawing
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public class Color
    {
        readonly CGColor color;

        public static Color RGB(nfloat red, nfloat green, nfloat blue)
        {
            return new Color(red, green, blue);
        }

        public static Color RGBA(nfloat red, nfloat green, nfloat blue, nfloat alpha)
        {
            return new Color(red, green, blue, alpha);
        }

        public Color(nfloat red, nfloat green, nfloat blue, nfloat alpha)
        {
            color = new CGColor(red, green, blue, alpha);
        }

        public Color(nfloat red, nfloat green, nfloat blue)
        {
            color = new CGColor(red, green, blue);
        }

        public Color(CGColor color)
        {
            this.color = color;
        }


        public CGColor AsCGColor => color;

        public NSColor AsNSColor => NSColor.FromCGColor(color);


        public override bool Equals(object other)
        {
            return color.Equals(other);
        }

        public override int GetHashCode()
        {
            return color.GetHashCode();
        }

        public static bool operator ==(Color c1, Color c2)
        {
            if (c1 is null) return c2 is null;

            return c1.Equals(c2);
        }

        public static bool operator !=(Color c1, Color c2)
        {
            return !(c1 == c2);
        }
    }


}

