﻿using System;
using AppKit;

namespace Einhugur.Drawing
{
    public class Font
    {
        public enum SystemControlFontSize
        {
            Regular = 0,
            Small = 1,
            Mini = 2,
            Large = 3
        }

        NSFont font;

        private Font(NSFont font)
        {
            this.font = font;
        }

        public Font(string name, nfloat size)
        {
            font = NSFont.FromFontName(name, size);
        }

        public Font(string family, nfloat size, bool bold)
        {
            NSFontManager fontManager = NSFontManager.SharedFontManager;

            font = fontManager.FontWithFamily(family, bold ? NSFontTraitMask.Bold : NSFontTraitMask.Unbold, 0, size);
        }

        public Font CreateBold()
        {
            return new Font(font.FamilyName, font.PointSize, true);
        }

        public static Font SystemFont(nfloat size) => new Font(NSFont.SystemFontOfSize(size));
        public static Font BoldSystemFont(nfloat size) => new Font(NSFont.BoldSystemFontOfSize(size));

        public static Font LabelFont(nfloat size) => new Font(NSFont.LabelFontOfSize(size));

        public static nfloat SystemFontSize => NSFont.SystemFontSize;
        public static nfloat LabelFontSize => NSFont.LabelFontSize;
        public static nfloat SmallSystemFontSize => NSFont.SmallSystemFontSize;
        public static nfloat SystemFontSizeForControl(SystemControlFontSize size) => NSFont.SystemFontSizeForControlSize((NSControlSize)size);

        public NSFont AsNSFont => font;

        public string Family => font.FamilyName;
        public string Name => font.FontName;
        public string DisplayName => font.DisplayName;
    }
}

