﻿using System;

namespace Einhugur.Arguments
{
    [FlagsAttribute]
    public enum KeyModifierFlags : ulong
    {
        AlphaShiftKey = 0x10000,
        ShiftKey = 0x20000,
        ControlKey = 0x40000,
        AlternateKey = 0x80000,
        CommandKey = 0x100000,
        NumericPadKey = 0x200000,
        HelpKey = 0x400000,
        FunctionKey = 0x800000,
        DeviceIndependentModifier = 0xFFFF0000
    }

    public class KeyEventArgs : EventArgs
    {
        private readonly ushort keyCode;
        private readonly string characters;
        private readonly string charactersIgnoringModifiers;
        private readonly KeyModifierFlags modifierFlags;
        private bool handled;

        public KeyEventArgs(ushort keyCode, string characters, string charactersIgnoringModifiers, KeyModifierFlags modifierFlags)
        {
            this.keyCode = keyCode;
            this.characters = characters;
            this.charactersIgnoringModifiers = charactersIgnoringModifiers;
            this.modifierFlags = modifierFlags;


        }

        public ushort KeyCode => keyCode;
        public string Characters => characters;
        public string CharactersIgnoringModifiers => charactersIgnoringModifiers;
        public KeyModifierFlags ModifierFlags => modifierFlags;

        public bool Handled
        {
            get => handled;
            set { handled = value; }
        }
    }
}

