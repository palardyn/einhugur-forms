﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Einhugur.Arguments
{
    [SuppressMessage("ReSharper", "ConvertToAutoProperty")]
    public class MouseEventArgs : EventArgs
    {
        private readonly nfloat x;
        private readonly nfloat y;

        public MouseEventArgs(nfloat x, nfloat y)
        {
            this.x = x;
            this.y = y;
        }

        public nfloat X => x;
        public nfloat Y => y;
    }

    public enum MouseButton
    {
        Left = 0,
        Right = 1
    }

    [SuppressMessage("ReSharper", "ConvertToAutoProperty")]
    public class MouseClickEventArgs : MouseEventArgs
    {
        private readonly MouseButton button;

        public MouseClickEventArgs(nfloat x, nfloat y, MouseButton button)
            : base(x, y)
        {
            this.button = button;
        }

        public MouseButton Button => button;
    }
    
    [SuppressMessage("ReSharper", "ConvertToAutoProperty")]
    public class MouseWheelEventArgs : MouseEventArgs
    {
        private readonly nfloat scrollingDeltaX;
        private readonly nfloat scrollingDeltaY;
        private readonly bool hasPreciseScrollingDeltas;
        public MouseWheelEventArgs(nfloat x, nfloat y, nfloat scrollingDeltaX, nfloat scrollingDeltaY, bool hasPreciseScrollingDeltas)
            : base(x, y)
        {
            this.scrollingDeltaX = scrollingDeltaX;
            this.scrollingDeltaY = scrollingDeltaY;
            this.hasPreciseScrollingDeltas = hasPreciseScrollingDeltas;
        }

        public nfloat ScrollingDeltaX => scrollingDeltaX;
        public nfloat ScrollingDeltaY => scrollingDeltaY;
        public bool HasPreciseScrollingDeltas => hasPreciseScrollingDeltas;
    }
}

