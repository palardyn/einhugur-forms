using System;
using System.Diagnostics.CodeAnalysis;
using Einhugur.Forms;

namespace Einhugur.Arguments
{
    [SuppressMessage("ReSharper", "ConvertToAutoProperty")]
    public class ErrorEventArgs : EventArgs
    {
        private readonly int code;
        private readonly string message;

        public ErrorEventArgs(int code, string message)
        {
            this.code = code;
            this.message = message;
        }

        public string Message => message;
        public int Code => code;
    }
    
    [SuppressMessage("ReSharper", "ConvertToAutoProperty")]
    public class NavigationErrorEventArgs : ErrorEventArgs
    {
        private readonly NavigationFailType failType;
        private readonly string reason;

        public NavigationErrorEventArgs(NavigationFailType failType, int code, string message, string reason)
            : base(code, message)
        {
            this.failType = failType;
            this.reason = reason;
        }

        public string Reason => reason;
        public NavigationFailType FailType => failType;
    }
    
    
}