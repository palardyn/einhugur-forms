using System;
using System.Diagnostics.CodeAnalysis;
using Einhugur.Forms;

namespace Einhugur.Arguments
{
    [SuppressMessage("ReSharper", "ConvertToAutoProperty")]
    public class UrlEventArgs : EventArgs
    {
        private readonly string url;

        public UrlEventArgs(string url)
        {
            this.url = url;
        }

        public string Url => url;
    }

    [SuppressMessage("ReSharper", "ConvertToAutoProperty")]
    public class UrlPolicyEventArgs : UrlEventArgs
    {
        private NavigationPolicy policy;
        
        public UrlPolicyEventArgs(string url, NavigationPolicy policy)
            : base(url)
        {
            this.policy = policy;
        }

        public NavigationPolicy Policy
        {
            get => policy;
            set => policy = value;
        }
    }
}