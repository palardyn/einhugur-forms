﻿using System;

namespace Einhugur.Arguments
{
    public class ActionEventArgs : EventArgs
    {
        private readonly bool invokedByUser;

        public ActionEventArgs(bool invokedByUser)
        {
            this.invokedByUser = invokedByUser;
        }

        public bool InvokedByUser => invokedByUser;
    }
}

