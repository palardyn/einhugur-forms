﻿using System;
namespace Einhugur.Arguments
{
    public class CellEventArgs : EventArgs
    {
        private readonly int columnIndex;
        private readonly int rowIndex;

        public CellEventArgs(int columnIndex, int rowIndex)
        {
            this.columnIndex = columnIndex;
            this.rowIndex = rowIndex;
        }

        public int ColumnIndex => columnIndex;
        public int RowIndex => rowIndex;
    }
}

