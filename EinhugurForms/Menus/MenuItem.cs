﻿using System;
using AppKit;
using Einhugur.Forms;

namespace Einhugur.Menus
{
    [Flags]
    public enum MenuKeyModifierMask : ulong
    {
        None = 0,
        Command = NSEventModifierMask.CommandKeyMask,
        Option = NSEventModifierMask.AlternateKeyMask,
        Shift = NSEventModifierMask.ShiftKeyMask
    }

    public class MenuItem : NSMenuItem
    {
        public MenuItem(string title, string charCode)
            : base(title, charCode, OnAction, OnValidate)
        {

        }

        public MenuItem(string title, string charCode, MenuKeyModifierMask keyModifiers)
            : base(title, charCode, OnAction, OnValidate)
        {
            this.KeyEquivalentModifierMask = (NSEventModifierMask)keyModifiers;
        }
        
        public static new NSMenuItem SeparatorItem => NSMenuItem.SeparatorItem;

        private static void OnAction(object sender, EventArgs e)
        {
            var window = NSApplication.SharedApplication.KeyWindow;

            var menuItem = (MenuItem)sender;

            bool handled = false;

            if (window != null)
            {
                // First we try to send event to a Window
                if (window is Window.FrameworkWindow)
                {
                    handled = (window as Window.FrameworkWindow).MenuAction(menuItem);
                }

            }

            if (!handled)
            {
                // Then we try to send to to our application
                var app = Application.Instance;

                if (app.MenuAction(menuItem))
                {
                    return;
                }
            }

            // If Window did not want to handle it and Application did not want it
            // then we try the MenuItem it self.
            menuItem.OnAction();

        }

        protected virtual void OnAction()
        {

        }

        
        private static bool OnValidate(NSMenuItem menuItem)
        {
            var window = NSApplication.SharedApplication.KeyWindow;

            if (window != null)
            {
                return window.ValidateMenuItem(menuItem);
            }
            var app = Application.Instance;

            return app.ValidateMenuItem(menuItem);
        }
    }
}

