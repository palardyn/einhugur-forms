﻿using AppKit;
using System.Collections.Generic;

namespace Einhugur.Menus
{
    public abstract class SpecialMenuItem : NSMenuItem
    {
        public enum SpecialCommand
        {
            Cut,
            Copy,
            Paste,
            SelectAll,
            Delete
        }

        private static readonly Dictionary<string, SpecialCommand> SpecialSelectors = new Dictionary<string, SpecialCommand>()
        {
            { "cut:", SpecialCommand.Cut },
            { "copy:", SpecialCommand.Copy },
            { "paste:", SpecialCommand.Paste },
            { "selectAll:", SpecialCommand.SelectAll },
            { "delete:", SpecialCommand.Delete }
        };

        public static bool IsSpecialSelector(string selector)
        {
            return SpecialSelectors.ContainsKey(selector);
        }

        public static SpecialCommand SpecialCommandFromSelector(string selector)
        {
            return SpecialSelectors[selector];
        }

        protected SpecialMenuItem(string title, ObjCRuntime.Selector selector, string charCode, MenuKeyModifierMask keyModifiers) :
            base(title, selector, charCode)
        {
            KeyEquivalentModifierMask = (NSEventModifierMask)keyModifiers;
        }
    }

    public class CutMenuItem : SpecialMenuItem
    {
        public CutMenuItem(string title, string charCode = "x", MenuKeyModifierMask keyModifiers = MenuKeyModifierMask.Command)
            : base(title, new ObjCRuntime.Selector("cut:"), charCode, keyModifiers)
        {

        }
    }

    public class CopyMenuItem : SpecialMenuItem
    {
        public CopyMenuItem(string title, string charCode = "c", MenuKeyModifierMask keyModifiers = MenuKeyModifierMask.Command)
            : base(title, new ObjCRuntime.Selector("copy:"), charCode, keyModifiers)
        {

        }
    }

    public class PasteMenuItem : SpecialMenuItem
    {
        public PasteMenuItem(string title, string charCode = "v", MenuKeyModifierMask keyModifiers = MenuKeyModifierMask.Command)
            : base(title, new ObjCRuntime.Selector("paste:"), charCode, keyModifiers)
        {
        }
    }

    public class SelectAllMenuItem : SpecialMenuItem
    {
        public SelectAllMenuItem(string title, string charCode = "a", MenuKeyModifierMask keyModifiers = MenuKeyModifierMask.Command)
            : base(title, new ObjCRuntime.Selector("selectAll:"), charCode, keyModifiers)
        {
        }
    }

    public class DeleteMenuItem : SpecialMenuItem
    {
        public DeleteMenuItem(string title, string charCode = "\u0008", MenuKeyModifierMask keyModifiers = MenuKeyModifierMask.Command)
            : base(title, new ObjCRuntime.Selector("delete:"), charCode, keyModifiers)
        {
        }
    }

    public sealed class ApplicationHideMenuItem : SpecialMenuItem
    {
        public ApplicationHideMenuItem(string title, string charCode = "h", MenuKeyModifierMask keyModifiers = MenuKeyModifierMask.Command)
            : base(title, new ObjCRuntime.Selector("hide:"), charCode, keyModifiers)
        {
            Target = NSApplication.SharedApplication;
        }
    }

    public sealed class ApplicationHideOthersMenuItem : SpecialMenuItem
    {
        public ApplicationHideOthersMenuItem(string title, string charCode = "h", MenuKeyModifierMask keyModifiers = MenuKeyModifierMask.Command | MenuKeyModifierMask.Option)
            : base(title, new ObjCRuntime.Selector("hideOtherApplications:"), charCode, keyModifiers)
        {
            Target = NSApplication.SharedApplication;
        }
    }

    public sealed class ApplicationShowAllMenuItem : SpecialMenuItem
    {
        public ApplicationShowAllMenuItem(string title, string charCode = "", MenuKeyModifierMask keyModifiers = MenuKeyModifierMask.Command | MenuKeyModifierMask.Option)
            // ReSharper disable once StringLiteralTypo
            : base(title, new ObjCRuntime.Selector("unhideAllApplications:"), charCode, keyModifiers)
        {
            Target = NSApplication.SharedApplication;
        }
    }

    public sealed class ApplicationServicesMenuItem : SpecialMenuItem
    {
        public ApplicationServicesMenuItem(string title, string charCode = "", MenuKeyModifierMask keyModifiers = MenuKeyModifierMask.Command | MenuKeyModifierMask.Option)
            : base(title, null, charCode, keyModifiers)
        {
            //Target = NSApplication.SharedApplication;

            var servicesMenu = new NSMenu(title: title);
            this.Submenu = servicesMenu;
            NSApplication.SharedApplication.ServicesMenu = servicesMenu;
        }
    }
    
    public sealed class AboutMenuItem : SpecialMenuItem
    {
        public AboutMenuItem(string title, string charCode = "")
            : base(title, new ObjCRuntime.Selector("orderFrontStandardAboutPanel:"), charCode, MenuKeyModifierMask.None)
        {
            Target = NSApplication.SharedApplication;
        }
    }
}

