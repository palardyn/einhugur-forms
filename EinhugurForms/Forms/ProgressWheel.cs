﻿using System;
using AppKit;
using static Einhugur.Forms.Button;

namespace Einhugur.Forms
{
	public class ProgressWheel : Control
	{

        public const int DefaultHeight = 16;
        public const int DefaultWidth = 16;

        internal class NSProgressWheelEx : NSProgressIndicator
        {
          
            public NSProgressWheelEx(CoreGraphics.CGRect frameRect)
                : base(frameRect)
            {
                this.Style = NSProgressIndicatorStyle.Spinning;
                this.Indeterminate = true;
                this.UsesThreadedAnimation = true;
            }
        }

        private bool is_Animating;

        public ProgressWheel(float x, float y) : this(x, y, DefaultWidth, DefaultHeight)
        { }

        public ProgressWheel(float x, float y, float width, float height)
        {
            Handle = new NSProgressWheelEx(new CoreGraphics.CGRect(x, y, width, height));
            is_Animating = false;    
		}

        public bool Threaded
        {
            get => ((NSProgressIndicator)Handle).UsesThreadedAnimation;
            set => ((NSProgressIndicator)Handle).UsesThreadedAnimation = value;
        }

        public new bool Visible
        {
            get => base.Visible;
            set
            {
                base.Visible = value;

                if (base.Visible)
                {
                    is_Animating = true;
                    ((NSProgressIndicator)Handle).StartAnimation((NSProgressIndicator)Handle);
                }
                else
                {
                    is_Animating = false;
                    ((NSProgressIndicator)Handle).StopAnimation((NSProgressIndicator)Handle);
                }
            }
        }

        public void StartAnimation()
        {
            if (base.Visible)
            {
                is_Animating = true;
                ((NSProgressIndicator)Handle).StartAnimation((NSProgressIndicator)Handle);
            }
        }

        public void StopAnimation()
        {
            is_Animating = false ;
            ((NSProgressIndicator)Handle).StopAnimation((NSProgressIndicator)Handle);
        }

        public bool Animating
        {
            get => is_Animating;
        }       
    }
}

