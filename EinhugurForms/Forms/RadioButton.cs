﻿using System;
using AppKit;
using CoreGraphics;
using Einhugur.Arguments;
using Einhugur.Drawing;
using Foundation;

namespace Einhugur.Forms
{
    public class RadioButton : Control
    {
        public const int DefaultHeight = 20;
        public const int DefaultWidth = 100;

        public event EventHandler GotFocus;
        public event EventHandler LostFocus;
        public event EventHandler<ActionEventArgs> Action;

        public RadioButton(float x, float y) : this(x, y, DefaultWidth, DefaultHeight)
        { }

        public RadioButton(float x, float y, float width, float height)
        {
            Handle = new Button.NSButtonEx(new CoreGraphics.CGRect(x, y, width, height), OnGotFocus, OnLostFocus);

            ((NSButton)Handle).SetButtonType(NSButtonType.Radio);

            ((NSButton)Handle).Activated += OnUserAction;
        }

        public string Caption
        {
            get => ((NSButton)Handle).Title;
            set => ((NSButton)Handle).Title = value;
        }

        public bool Enabled
        {
            get => ((NSButton)Handle).Enabled;
            set => ((NSButton)Handle).Enabled = value;
        }

        public string ToolTip
        {
            get => ((NSButton) Handle).ToolTip;
            set => ((NSButton) Handle).ToolTip = value;
        }

        public bool Value
        {
            get => ((NSButton)Handle).State == NSCellStateValue.On;
            set
            {
                if (value != Value)
                {
                    ((NSButton)Handle).State = value ? NSCellStateValue.On : NSCellStateValue.Off;
                    OnAction(false);
                }
            }
        }

        protected virtual void OnAction(bool invokedByUser)
        {
            Action?.Invoke(this, new ActionEventArgs(invokedByUser));
        }

        private void OnUserAction(object sender, EventArgs e)
        {
            OnAction(true);
        }

        protected virtual void OnGotFocus()
        {
            GotFocus?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnLostFocus()
        {
            LostFocus?.Invoke(this, EventArgs.Empty);
        }

        public override void Dispose()
        {
            ((NSButton)Handle).Activated -= OnUserAction;
            Action = null;
            GotFocus = null;
            LostFocus = null;

            base.Dispose();
        }
        
        public override void DrawInto(GraphicsContext g, float x, float y)
        {
            NSButton btn = new NSButton(new CGRect(0, 0, this.Width, this.Height));
            btn.WantsLayer = true;
            btn.SetButtonType(NSButtonType.Radio);
            btn.Title = this.Caption;
            btn.State = ((NSButton) Handle).State;
            btn.Enabled = this.Enabled;
            
            g.Handle.SaveGraphicsState();
            var tr = new NSAffineTransform();
            tr.Translate(x, y);
            tr.Concat();
            
            //btn.DisplayRect(new CGRect(0, 0, this.Width, this.Height) );
            btn.DisplayRectIgnoringOpacity(new CGRect(0, 0, this.Width, this.Height), g.Handle);

            g.Handle.RestoreGraphicsState();
            
            // Alternate way:
            /*var rep = btn.BitmapImageRepForCachingDisplayInRect(btn.Bounds);
            btn.CacheDisplay(btn.Bounds, rep);
        
            var img = new NSImage(btn.Bounds.Size);
            img.Flipped = true;
            img.AddRepresentation(rep);

            var old = NSGraphicsContext.CurrentContext;
            NSGraphicsContext.CurrentContext = g.Handle;
            img.Draw(new CGPoint(x, y), CGRect.Empty, NSCompositingOperation.Copy, 1.0f);
            NSGraphicsContext.CurrentContext = old;*/
        }
    }
}

