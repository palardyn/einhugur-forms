﻿using System;
using AppKit;

namespace Einhugur.Forms
{
    public class Button : Control
    {
        public const int DefaultHeight = 20;
        public const int DefaultWidth = 80;

        internal class NSButtonEx : NSButton
        {
            readonly Action gotFocusHandler;
            readonly Action lostFocusHandler;

            public NSButtonEx(CoreGraphics.CGRect frameRect, Action gotFocusHandler, Action lostFocusHandler)
                : base(frameRect)
            {
                this.gotFocusHandler = gotFocusHandler;
                this.lostFocusHandler = lostFocusHandler;

                //this.Transparent = false;
                //this.Bordered = true;
                //this.Appearance = NSAppearance.GetAppearance(NSAppearance.NameAqua);
            }

            public override bool BecomeFirstResponder()
            {
                gotFocusHandler();

                return base.BecomeFirstResponder();
            }

            public override bool ResignFirstResponder()
            {
                lostFocusHandler();

                return base.ResignFirstResponder();
            }

            //public override bool IsOpaque => true;

            //public override bool AllowsVibrancy => false;
        }


        public event EventHandler Pressed;
        public event EventHandler GotFocus;
        public event EventHandler LostFocus;
        
        internal Button(NSButton button)
        {
            this.Handle = button;
        }

        public bool DestructiveAction
        {
            get => ((NSButton) Handle).HasDestructiveAction;
            set => ((NSButton) Handle).HasDestructiveAction = value;
        }
        
        public bool IsDefaultButton
        {
            get => ((NSButton) Handle).KeyEquivalent == "\r";
            set
            {
                if(value)
                {
                    ((NSButton)Handle).KeyEquivalent = "\r";
                }
                else
                {
                    if(!IsCancelButton)
                    {
                        ((NSButton)Handle).KeyEquivalent = "";
                    }
                }
            }
        }
        
        public bool IsCancelButton
        {
            get => ((NSButton) Handle).KeyEquivalent == ((char)27).ToString();
            set
            {
                if (value)
                {
                    ((NSButton)Handle).KeyEquivalent = ((char)27).ToString();
                }
                else
                {
                    if (!IsDefaultButton)
                    {
                        ((NSButton)Handle).KeyEquivalent = "";
                    }
                }
            }
        }
        
        public string ToolTip
        {
            get => ((NSButton) Handle).ToolTip;
            set => ((NSButton) Handle).ToolTip = value;
        }

        public Button(float x, float y) : this(x, y, DefaultWidth, DefaultHeight)
        { }

        public Button(float x, float y, float width, float height)
        {
            Handle = new NSButtonEx(new CoreGraphics.CGRect(x, y, width, height), OnGotFocus, OnLostFocus)
            {
                BezelStyle = NSBezelStyle.Rounded
            };

            ((NSButton)Handle).Activated += OnPressed;
        }

        protected virtual void OnGotFocus()
        {
            GotFocus?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnLostFocus()
        {
            LostFocus?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnPressed(object sender, EventArgs e)
        {
            Pressed?.Invoke(this, EventArgs.Empty);
        }

        public string Caption
        {
            get => ((NSButton)Handle).Title;
            set => ((NSButton)Handle).Title = value; 
        }

        public bool Enabled
        {
            get => ((NSButton)Handle).Enabled; 
            set => ((NSButton)Handle).Enabled = value;
        }

        public override void Dispose()
        {
            ((NSButton)Handle).Activated -= OnPressed;
            Pressed = null;
            GotFocus = null;
            LostFocus = null;

            base.Dispose();
        }
    }
}

