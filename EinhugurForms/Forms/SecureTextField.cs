using System;
using AppKit;

namespace Einhugur.Forms
{
    public class SecureTextField : TextField
    {
        private class NSTextFieldEx : NSSecureTextField
        {
            private readonly WeakReference<SecureTextField> owner;

            public NSTextFieldEx(CoreGraphics.CGRect frameRect, SecureTextField textField)
                : base(frameRect)
            {
                owner = new WeakReference<SecureTextField>(textField);
            }

            public override bool BecomeFirstResponder()
            {
                if (owner.TryGetTarget(out var textField))
                {
                    textField.OnGotFocus();
                }
                
                return base.BecomeFirstResponder();
            }

            public override bool ResignFirstResponder()
            {
                if (owner.TryGetTarget(out var textField))
                {
                    textField.OnLostFocus();
                }

                return base.ResignFirstResponder();
            }
        }

        public SecureTextField(float x, float y) : this(x, y, DefaultWidth, DefaultHeight)
        { }

        public SecureTextField(float x, float y, float width, float height)
            : base()
        {
            Handle = new NSTextFieldEx(new CoreGraphics.CGRect(x, y, width, height), this)
            {
                BezelStyle = NSTextFieldBezelStyle.Square
            };
            
            ((NSTextField)Handle).Changed += OnTextChanged;
        }
    }
}