﻿using System;
using System.Diagnostics.CodeAnalysis;
using Einhugur.Arguments;
using Foundation;
using WebKit;

namespace Einhugur.Forms
{
    public enum NavigationPolicy : long
    {
        Allow = WKNavigationActionPolicy.Allow,
        Cancel = WKNavigationActionPolicy.Cancel,
        Download = WKNavigationActionPolicy.Download
    }
    
    public enum NavigationFailType
    {
        FailedNavigation = 0,
        FailedProvisionalNavigation = 1,
        WebViewContentProcessTerminated = 2
    }

 
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public class WebBrowserControl : Control
    {
        public const int DefaultHeight = 200;
        public const int DefaultWidth = 300;

        internal class WKWebViewEx : WKWebView
        {
            readonly WeakReference<WebBrowserControl> owner;

            public WKWebViewEx(CoreGraphics.CGRect frameRect, WKWebViewConfiguration configuration, WebBrowserControl owner)
                    : base(frameRect, configuration)
            {
                this.owner = new WeakReference<WebBrowserControl>(owner);
            }

            public override bool BecomeFirstResponder()
            {
                if (owner.TryGetTarget(out WebBrowserControl browser))
                {
                    browser.OnGotFocus();
                }

                return base.BecomeFirstResponder();
            }

            public override bool ResignFirstResponder()
            {
                if (owner.TryGetTarget(out WebBrowserControl browser))
                {
                    browser.OnLostFocus();
                }

                return base.ResignFirstResponder();
            }
        }

        public event EventHandler GotFocus;
        public event EventHandler LostFocus;
        public event EventHandler<UrlEventArgs> NavigationCompleted;
        public event EventHandler<UrlEventArgs> NavigationStarted;
        public event EventHandler<UrlPolicyEventArgs> DecidePolicy;
        public event EventHandler<NavigationErrorEventArgs> NavigationError;
        
        private class WKNavigationDelegateEx : WKNavigationDelegate
        {
            private readonly WeakReference<WebBrowserControl> browser;
            private NSUrl url;

            public WKNavigationDelegateEx(WebBrowserControl browser)
            {
                this.browser = new WeakReference<WebBrowserControl>(browser);
                url = null;
            }
            
            public override void DidFinishNavigation(WKWebView webView, WKNavigation navigation)
            {
                if (browser.TryGetTarget(out var owner))
                {
                    owner.OnNavigationCompleted();
                }
            }

            public override void DidCommitNavigation(WKWebView webView, WKNavigation navigation)
            {
                
                if (browser.TryGetTarget(out var owner))
                {
                    owner.OnNavigationStarted(url.AbsoluteString);
                }
            }

            public override void DecidePolicy(WKWebView webView, WKNavigationAction navigationAction, Action<WKNavigationActionPolicy> decisionHandler)
            {
                url = navigationAction.Request.Url;
                
                if (browser.TryGetTarget(out var owner))
                {
                    var policy = owner.OnDecidePolicy(url.AbsoluteString);
                    decisionHandler((WKNavigationActionPolicy)policy);
                    return;
                }
                
                // IF we did not handle it then we allow it by default
                decisionHandler(WKNavigationActionPolicy.Allow);
            }

            public override void DidFailNavigation(WKWebView webView, WKNavigation navigation, NSError error)
            {
                if (browser.TryGetTarget(out var owner))
                {
                    owner.OnNavigationError(NavigationFailType.FailedNavigation, (int)error.Code, error.LocalizedDescription, error.LocalizedFailureReason);
                }
            }

            public override void DidFailProvisionalNavigation(WKWebView webView, WKNavigation navigation, NSError error)
            {
                if (browser.TryGetTarget(out var owner))
                {
                    owner.OnNavigationError(NavigationFailType.FailedProvisionalNavigation, (int)error.Code, error.LocalizedDescription, error.LocalizedFailureReason);
                }
            }

            public override void ContentProcessDidTerminate(WKWebView webView)
            {
                if (browser.TryGetTarget(out var owner))
                {
                    owner.OnNavigationError(NavigationFailType.WebViewContentProcessTerminated, -9000, "Content process terminated", "Unknown");
                }
            }
        }

        public WebBrowserControl(float x, float y) : this(x, y, DefaultWidth, DefaultHeight)
        { }

        public WebBrowserControl(float x, float y, float width, float height)
        {
            var webConfiguration = new WKWebViewConfiguration();
            Handle = new WKWebViewEx(new CoreGraphics.CGRect(x, y, width, height), webConfiguration, this);

            ((WKWebView) Handle).NavigationDelegate = new WKNavigationDelegateEx(this);
        }

        public bool IsLoading => ((WKWebView) Handle).IsLoading;

        public double EstimatedProgress => ((WKWebView) Handle).EstimatedProgress;
        
        public bool CanGoBack => ((WKWebView) Handle).CanGoBack;
        public bool CanGoForward => ((WKWebView) Handle).CanGoForward;
        
        public bool HasOnlySecureContent => ((WKWebView) Handle).HasOnlySecureContent;
        
        public string Title => ((WKWebView) Handle).Title;
        
        public bool AllowsBackForwardNavigationGestures
        {
            get => ((WKWebView) Handle).AllowsBackForwardNavigationGestures;
            set => ((WKWebView) Handle).AllowsBackForwardNavigationGestures = value;
        }
        
        public bool AllowsLinkPreview
        {
            get => ((WKWebView) Handle).AllowsLinkPreview;
            set => ((WKWebView) Handle).AllowsLinkPreview = value;
        }
        
        public bool AllowsMagnification
        {
            get => ((WKWebView) Handle).AllowsMagnification;
            set => ((WKWebView) Handle).AllowsMagnification = value;
        }
        
        public nfloat Zoom
        {
            get => ((WKWebView) Handle).PageZoom;
            set => ((WKWebView) Handle).PageZoom = value;
        }
        
        public nfloat Magnification
        {
            get => ((WKWebView) Handle).Magnification;
            set => ((WKWebView) Handle).Magnification = value;
        }


        public string Url
        {
            get
            {
                var nsUrl = ((WKWebView) Handle).Url;
                return (!(nsUrl is null)) ? nsUrl.AbsoluteString : "";
            }
        }

        protected virtual void OnNavigationError(NavigationFailType failType, int code, string message, string reason)
        {
            NavigationError?.Invoke(this, new NavigationErrorEventArgs(failType, code, message, reason));
        }

        protected virtual void OnNavigationCompleted()
        {
            NavigationCompleted?.Invoke(this, new UrlEventArgs(Url));
        }
        
        protected virtual void OnNavigationStarted(string url)
        {
            NavigationStarted?.Invoke(this, new UrlEventArgs(url));
        }

        protected virtual NavigationPolicy OnDecidePolicy(string url)
        {
            var args = new UrlPolicyEventArgs(url, NavigationPolicy.Allow);
            
            DecidePolicy?.Invoke(this, args);

            return args.Policy;
        }

        

        public void GoBack()
        {
            ((WKWebView) Handle).GoBack();
        }
        
        public void GoForward()
        {
            ((WKWebView) Handle).GoForward();
            
            
        }
        
        public void StopLoading()
        {
            ((WKWebView) Handle).StopLoading();
        }
        
        public void Reload()
        {
            ((WKWebView) Handle).Reload();
        }
        
        public void LoadRequest(string url)
        {
            ((WKWebView)Handle).LoadRequest(NSUrlRequest.FromUrl(new NSUrl(url)));
        }

        public void LoadHtmlString(string htmlString, string baseUrl)
        {
            ((WKWebView)Handle).LoadHtmlString(htmlString, new NSUrl(baseUrl));
        }

        public void LoadFile(FileSystemItem f)
        {
            if (f == null) throw new ArgumentNullException(nameof(f));
            if (!f.Exists) throw new ArgumentException("f does not exist");
            if (f.IsDirectory) throw new ArgumentException("f cannot be directory");

            ((WKWebView) Handle).LoadFileUrl(
                new NSUrl(f.FullPath, false),
                new NSUrl(f.Parent.FullPath, true));
        }

        protected virtual void OnGotFocus()
        {
            GotFocus?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnLostFocus()
        {
            LostFocus?.Invoke(this, EventArgs.Empty);
        }

        public override void Dispose()
        {
            GotFocus = null;
            LostFocus = null;
            NavigationCompleted = null;
            NavigationStarted = null;
            DecidePolicy = null;
            NavigationError = null;

            base.Dispose();
        }
    }
}