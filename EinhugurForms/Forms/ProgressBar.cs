using AppKit;

namespace Einhugur.Forms
{
    public class ProgressBar : Control
    {
        public enum ProgressBarStyle : ulong
        {
            Bar = NSProgressIndicatorStyle.Bar,
            Circular = NSProgressIndicatorStyle.Spinning
        }

        public const int DefaultHeight = 20;
        public const int DefaultWidth = 100;

        public ProgressBar(float x, float y) : this(x, y, DefaultWidth, DefaultHeight)
        { }

        public ProgressBar(float x, float y, float width, float height)
        {
            Handle = new NSProgressIndicator(new CoreGraphics.CGRect(x, y, width, height));
        }
        
        public ProgressBarStyle Style
        {
            get => (ProgressBarStyle)((NSProgressIndicator) Handle).Style;
            set => ((NSProgressIndicator) Handle).Style = (NSProgressIndicatorStyle)value;
        }

        public double MinValue
        {
            get => ((NSProgressIndicator) Handle).MinValue;
            set => ((NSProgressIndicator) Handle).MinValue = value;
        }
        
        public double MaxValue
        {
            get => ((NSProgressIndicator) Handle).MaxValue;
            set => ((NSProgressIndicator) Handle).MaxValue = value;
        }
        
        public double Value
        {
            get => ((NSProgressIndicator) Handle).DoubleValue;
            set => ((NSProgressIndicator) Handle).DoubleValue = value;
        }
        
        public bool Indeterminate
        {
            get => ((NSProgressIndicator) Handle).Indeterminate;
            set => ((NSProgressIndicator) Handle).Indeterminate = value;
        }
        
        public bool DisplayedWhenStopped
        {
            get => ((NSProgressIndicator) Handle).IsDisplayedWhenStopped;
            set => ((NSProgressIndicator) Handle).IsDisplayedWhenStopped = value;
        }

        public void StartIndeterminate()
        {
            if (!((NSProgressIndicator) Handle).Indeterminate)
            {
                ((NSProgressIndicator) Handle).Indeterminate = true;
            }
            ((NSProgressIndicator) Handle).StartAnimation(null);
        }
        
        public void StopIndeterminate()
        {
            if (((NSProgressIndicator) Handle).Indeterminate)
            {
                ((NSProgressIndicator) Handle).StopAnimation(null);
            }
            
        }

        public string ToolTip
        {
            get => ((NSProgressIndicator)Handle).ToolTip;
            set => ((NSProgressIndicator)Handle).ToolTip = value;
        }
    }
}