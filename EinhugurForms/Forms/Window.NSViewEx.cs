using System;
using AppKit;
using CoreGraphics;
using Einhugur.Arguments;
using Einhugur.Drawing;

namespace Einhugur.Forms
{
    public abstract partial class Window
    {
        internal class NSViewEx : NSView
        {
            readonly WeakReference<Window> owner;
            public bool mouseTracking;
            

            public NSViewEx(CGRect bounds, Window owner)
                : base(bounds)
            {
                this.owner = new WeakReference<Window>(owner);
            }

            public override bool IsFlipped => true;

            public override void DrawRect(CGRect dirtyRect)
            {
                NSGraphicsContext ctx = NSGraphicsContext.CurrentContext;
                ctx?.SaveGraphicsState();

                if (owner.TryGetTarget(out var window))
                {
                    using (var g = new GraphicsContext(ctx, this.Frame.Size))
                    {
                        window.Paint(g);
                    }

                }

                ctx?.RestoreGraphicsState();
            }

            internal Window Owner
            {
                get
                {
                    if(owner.TryGetTarget(out Window window))
                    {
                        return window;
                    }
                    return null;
                }
            }
            
            public override void MouseEntered(NSEvent theEvent)
            {
                if (owner.TryGetTarget(out var window))
                {
                    window.MouseEntered();
                }
            }

            public override void MouseExited(NSEvent theEvent)
            {
                if (owner.TryGetTarget(out var window))
                {
                    window.MouseExited();
                }
            }
            
            public override void MouseMoved(NSEvent theEvent)
            {
                if (owner.TryGetTarget(out var window))
                {
                    var local = this.ConvertPointFromView(theEvent.LocationInWindow, null);

                    window.MouseMoved(local.X, local.Y);
                }
            }
            
            public override void UpdateTrackingAreas()
            {
                base.UpdateTrackingAreas();

                if (mouseTracking)
                {
                    foreach (var item in TrackingAreas())
                    {
                        RemoveTrackingArea(item);
                    }

                    var options = NSTrackingAreaOptions.MouseEnteredAndExited | NSTrackingAreaOptions.ActiveAlways;

                    var trackingArea = new NSTrackingArea(this.Bounds, options, this, null);

                    AddTrackingArea(trackingArea);
                }
            }
            
            public override void ScrollWheel(NSEvent theEvent)
            {
                if (theEvent.DeltaY != 0)
                {
                    if (owner.TryGetTarget(out Window window))
                    {
                        var local = this.ConvertPointFromView(theEvent.LocationInWindow, null);
                        
                        window.MouseWheel(local.X,local.Y, theEvent.ScrollingDeltaX, theEvent.ScrollingDeltaY, theEvent.HasPreciseScrollingDeltas);
                    }
                    
                }

                base.ScrollWheel(theEvent);
            }

            public override void MouseDown(NSEvent theEvent)
            {
                if (owner.TryGetTarget(out Window window))
                {
                    var local = this.ConvertPointFromView(theEvent.LocationInWindow, null);

                    window.MouseDown(local.X, local.Y, MouseButton.Left);
                }
            }

            public override void MouseUp(NSEvent theEvent)
            {
                if (owner.TryGetTarget(out Window window))
                {
                    var local = this.ConvertPointFromView(theEvent.LocationInWindow, null);

                    window.MouseUp(local.X, local.Y, MouseButton.Left);
                }
            }

            public override void RightMouseDown(NSEvent theEvent)
            {
                if (owner.TryGetTarget(out Window window))
                {
                    var local = this.ConvertPointFromView(theEvent.LocationInWindow, null);

                    window.MouseDown(local.X, local.Y, MouseButton.Right);
                }
            }

            public override void RightMouseUp(NSEvent theEvent)
            {
                if (owner.TryGetTarget(out Window window))
                {
                    var local = this.ConvertPointFromView(theEvent.LocationInWindow, null);

                    window.MouseUp(local.X, local.Y, MouseButton.Right);
                }
            }
        }
    }
}