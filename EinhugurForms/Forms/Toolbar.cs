﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using AppKit;
using CoreGraphics;
using Einhugur.Arguments;
using Foundation;
using ObjCRuntime;
using ScreenCaptureKit;
using static Einhugur.Forms.Window;

namespace Einhugur.Forms
{
    [Flags]
    public enum ToolbarDisplayMode : ulong
    {
        Default = NSToolbarDisplayMode.Default,
        Icon = NSToolbarDisplayMode.Icon,
        IconAndLabel = NSToolbarDisplayMode.IconAndLabel,
        Label = NSToolbarDisplayMode.Label
    }


    public class Toolbaritem : NSToolbarItem
    {
        public event EventHandler<EventArgs> Action;
                
        public Toolbaritem(string name) : base(name)
        {

            this.Action += OnUserAction;

        }

        private void OnUserAction(object sender, EventArgs e)
        {
            Action?.Invoke(this, EventArgs.Empty);
        }

        public string Name
        {
            get => Identifier;
        }
    }

    class ToolbarDelegate : NSToolbarDelegate
    {
        public string[] AllowedItemIdentifiers(NSToolbar toolbar)
        {
            List<string> identifiers = new List<string>();

            identifiers.Add("Run");
            identifiers.Add(NSToolbar.NSToolbarPrintItemIdentifier);
            identifiers.Add(NSToolbar.NSToolbarShowColorsItemIdentifier);

            return identifiers.ToArray();
        }

        public string[] DefaultItemIdentifiers(NSToolbar toolbar)
        {
            List<string> identifiers = new List<string>();

            identifiers.Add("Run");
            identifiers.Add(NSToolbar.NSToolbarPrintItemIdentifier);
            
            return identifiers.ToArray();
        }

        public NSToolbarItem WillInsertItem(NSToolbar toolbar, string itemIdentifier, bool willBeInserted)
        {

            if ("Run" == itemIdentifier)
            {
                return new Toolbaritem("Run");
            }

            return null;

        }
        public bool GetItemCanBeInsertedAt(NSToolbar toolbar, string itemIdentifier, nint index)
        {
            return true;
        }

        public NSSet<NSString> GetToolbarImmovableItemIdentifiers(NSToolbar toolbar)
        {
            return null;
        }

        public string[] SelectableItemIdentifiers(NSToolbar toolbar)
        {
            return DefaultItemIdentifiers(toolbar);
        }


        public void DidRemoveItem(NSNotification notification)
        {
        
        }       

        public void WillAddItem(NSNotification notification)
        {
        
        }
               
    }

    public class Toolbar : NSToolbar
	{
        
        public Toolbar(string toolbarName) : base(toolbarName)
		{

            Visible = true;
            Delegate = new ToolbarDelegate();
            AllowsUserCustomization = true;
            AddItem(new Toolbaritem("Run") );
            AddItem(new Toolbaritem(NSToolbar.NSToolbarPrintItemIdentifier));

            ValidateVisibleItems();
           
        }

        public ToolbarDisplayMode DisplayMode
        {
            // displayMode NSToolbarDisplayMode displayMode;
            get => (ToolbarDisplayMode)((NSToolbar)this).DisplayMode;
            set => ((NSToolbar)this).DisplayMode = (NSToolbarDisplayMode)value;
        }

        // we do not need to override
            // showsBaselineSeparator
            // allowsUserCustomization
            // items[] - all items (including the overflow menu)
            // visibleItems - those that are visible (will excluse overflow items)
            // selectedItemIdentifier

        // runCustomizationPalette:

        //// when we add an item we want to set it up to run an event
        //// in the toolbar - ItemAction & we pass the item back to the user ?
        public void AddItem( Toolbaritem newItem )
        {
            InsertItem(newItem.Name, Items.Length);
        }

        // - (void)removeItemAtIndex:(NSInteger)index;


    }
}
