﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using AppKit;

namespace Einhugur.Forms
{
    public abstract class ContainerControl : Control
    {
        private List<Control> controls = new List<Control>();

        internal class ContentViewEx : NSView
        {
            public override bool IsFlipped => true;
        }

        internal override void Activate()
        {
            base.Activate();

            foreach (var control in controls)
            {
                control.Activate();
            }
        }

        internal override void Deactivate()
        {
            base.Deactivate();
            
            foreach (var control in controls)
            {
                control.Deactivate();
            }
        }

        protected void SetupControlsForView(NSView view, Func<IEnumerable<Control>> setupControls = null)
        {
            view.TranslatesAutoresizingMaskIntoConstraints = true;

            var controlEnumerable = setupControls?.Invoke();

            if (controlEnumerable != null)
            {
                foreach (var control in controlEnumerable)
                {
                    AddControl(control, view);
                }
            }
        }

        private void AddControl(Control control, NSView view)
        {
            controls.Add(control);

            view.AddSubview(control.Handle);
            control.SetParentControl(this);

            if (control.LockLeft && control.LockRight)
            {
                control.Handle.AutoresizingMask |= NSViewResizingMask.WidthSizable;
            }
            else if (control.LockRight)
            {
                control.Handle.AutoresizingMask |= NSViewResizingMask.MinXMargin;
            }

            if (control.LockTop && control.LockBottom)
            {
                control.Handle.AutoresizingMask |= NSViewResizingMask.HeightSizable;
            }
            else if (control.LockBottom)
            {
                control.Handle.AutoresizingMask |= NSViewResizingMask.MinYMargin;
            }
        }

        public void AddControl(Control control)
        {
            AddControl(control, Handle);
        }

        internal void RemoveControl(Control control)
        {
            controls.Remove(control);
        }


        protected void SetupControls(Func<IEnumerable<Control>> setupControls = null)
        {
            SetupControlsForView(Handle, setupControls);
        }


        /// <summary>
        /// For internal framework use, do not try to call this method. 
        /// </summary>
        /// <param name="parentWindow"></param>
        [EditorBrowsable(EditorBrowsableState.Never)]
        internal override void SetParentWindow(Window parentWindow)
        {
            base.SetParentWindow(parentWindow);

            if (controls != null)
            {
                foreach (var control in controls)
                {
                    control.SetParentWindow(parentWindow);
                }
            }
        }

        public override void Dispose()
        {
            foreach(var control in controls)
            {
                control.Dispose();
            }

            base.Dispose();
        }
    }
}

