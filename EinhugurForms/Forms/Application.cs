﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using AppKit;
using Einhugur.Arguments;
using Einhugur.Menus;
using Foundation;
using System.Linq;

namespace Einhugur.Forms
{
    public enum ApplicationReply : ulong
    {
        Success = NSApplicationDelegateReply.Success,
        Cancel = NSApplicationDelegateReply.Cancel,
        Failure = NSApplicationDelegateReply.Failure

    }

    public enum ApplicationAppearance
    {
        SystemDefault,
        Dark,
        Light
    }


    [SuppressMessage("ReSharper", "MemberCanBeProtected.Global")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public abstract class Application
    {
        private Dictionary<MenuItem, EventHandler<MenuEventArgs>> menuHandlers;

        private static Application instance;

        public static Application Instance => instance;

        public ApplicationAppearance Appearance
        {
            get
            {
                if (NSApplication.SharedApplication.Appearance == null)
                {
                    return ApplicationAppearance.SystemDefault;
                }
                else if(NSApplication.SharedApplication.Appearance.Name == NSAppearance.NameAqua)
                {
                    return ApplicationAppearance.Light;
                }
                else
                {
                    return ApplicationAppearance.Dark;
                }
            }
            set
            {
                NSApplication.SharedApplication.Appearance = (value == ApplicationAppearance.SystemDefault) ? null :
                    (value == ApplicationAppearance.Dark) ? NSAppearance.GetAppearance(NSAppearance.NameDarkAqua) :
                    NSAppearance.GetAppearance(NSAppearance.NameAqua);
            }
        }

        public IEnumerable<Window> Windows
        {
            get
            {
                var windows = NSApplication.SharedApplication.DangerousWindows;

                var result = new List<Window>();
             
                foreach (var window in (IEnumerable<NSWindow>)windows)
                {
                    if(window is Window.FrameworkWindow)
                    {
                        var wnd = (window as Window.FrameworkWindow).Owner;

                        if(wnd != null && !wnd.HasBeenClosed)
                        {
                            result.Add(wnd);
                        }
                    }
                }

                return result;
            }
        }

        public IEnumerable<T> WindowsOfType<T>() where T : Window
        {
            var windows = NSApplication.SharedApplication.DangerousWindows;

            var result = new List<T>();

            foreach (var window in (IEnumerable<NSWindow>)windows)
            {
                if (window is Window.FrameworkWindow )
                {
                    var wnd = (window as Window.FrameworkWindow).Owner;

                    if (wnd != null && wnd is T && !wnd.HasBeenClosed)
                    {
                        result.Add((T)wnd);
                    }
                }
            }

            return result;
        }


        private class AppDelegate : NSApplicationDelegate
        {
            readonly Application application;

            public AppDelegate(Application application)
            {
                this.application = application;
            }

            public override void WillTerminate(NSNotification notification)
            {
                this.application.Closing();
            }

            [Export("application:openFile:")]
            public override bool OpenFile(NSApplication sender, string filename)
            {
                return application.OpenFile(new FileSystemItem(filename));
            }

            [Export("application:openFiles:")]
            public override void OpenFiles(NSApplication sender, string[] filenames)
            {
                NSApplication.SharedApplication.ReplyToOpenOrPrint(
                    (NSApplicationDelegateReply)application.OpenFiles(filenames.Select(x => new FileSystemItem(x)).ToArray()));
            }

            public override void DidFinishLaunching(NSNotification notification)
            {
                application.Opened();
            }
        }

        protected static void Init(Type applicationClass, string[] args)
        {
            NSApplication.Init();

            object instanceType = Activator.CreateInstance(applicationClass);

            Application.instance = instanceType as Application ?? throw new ApplicationException("Class type used does not inherit from Einhugur.Forms.Application");

            NSApplication.Main(args);
        }

        protected Application()
        {
            instance = this;
            NSApplication.SharedApplication.Delegate = new AppDelegate(this);

            Opening();
        }

        public Dictionary<MenuItem, EventHandler<MenuEventArgs>> MenuHandlers
        {
            get => menuHandlers;
            set => menuHandlers = value;
        }

        public string LocalizedDisplayName => 
        NSBundle.MainBundle.ObjectForInfoDictionary(("CFBundleDisplayName"))?.ToString() ??
            NSBundle.MainBundle.ObjectForInfoDictionary(("CFBundleName"))?.ToString();

        public static void ShowURL(string url) => NSWorkspace.SharedWorkspace.OpenUrl(new NSUrl(url));

        public static string Version => NSBundle.MainBundle.ObjectForInfoDictionary(("CFBundleShortVersionString"))?.ToString();

        internal bool MenuAction(MenuItem item)
        {
            bool handled = false;

            if (menuHandlers != null && menuHandlers.ContainsKey(item))
            {
                var args = new MenuEventArgs();
                menuHandlers[item]?.Invoke(item, args);

                handled = args.Handled;
            }

            return handled;
        }

        protected virtual void Opened()
        {

        }

        protected virtual void Opening()
        {

        }

        protected virtual void Closing()
        {

        }

        protected virtual bool OpenFile(FileSystemItem file)
        {
            return false;
        }

        protected virtual ApplicationReply OpenFiles(FileSystemItem[] files)
        {
            return ApplicationReply.Failure;
        }

        public void Quit()
        {
            NSApplication.SharedApplication.Terminate(NSApplication.SharedApplication);
        }

        public virtual bool ValidateMenuItem(NSMenuItem menuItem)
        {
            // this is basically the same as a Xojo Applications EnableMenuItems event

            return true;
        }
    }
}

