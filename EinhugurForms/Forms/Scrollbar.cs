using System;
using System.Diagnostics.CodeAnalysis;
using AppKit;
using CoreGraphics;
using Einhugur.Arguments;
using Foundation;

namespace Einhugur.Forms
{
    public class Scrollbar : Control
    {
        public const int DefaultHeight = 16;
        public const int DefaultWidth = 200;

        private int maximum;
        private int minimum;
        private int pageStep;
        private int lineStep;
        private bool enabled;
        private int value;

        public event EventHandler<ActionEventArgs> ValueChanged;

        private sealed class NSScrollerEx : NSScroller
        {
            public bool liveScrolling;
            private readonly WeakReference<Scrollbar> owner;

            public NSScrollerEx(CGRect frameRect, Scrollbar owner)
                : base(frameRect.Height > frameRect.Width ?
                    new CGRect(frameRect.X, frameRect.Y, NSScroller.ScrollerWidth, frameRect.Height) :
                    new CGRect(frameRect.X, frameRect.Y, frameRect.Width, NSScroller.ScrollerWidth))
            {
                Target = this;
                Action = new ObjCRuntime.Selector("scrollerAction:");

                this.owner = new WeakReference<Scrollbar>(owner);

                liveScrolling = true;
            }
            
            [Export("scrollerAction:")]
            [SuppressMessage("ReSharper", "InconsistentNaming")]
            [SuppressMessage("ReSharper", "UnusedMember.Local")]
            [SuppressMessage("ReSharper", "UnusedParameter.Local")]
            public void selectionChanged(NSObject sender)
            {
                if (!owner.TryGetTarget(out var scroller))
                {
                    return;
                }
                
                switch (this.HitPart)
                {
                    case NSScrollerPart.DecrementLine:
                        scroller.SetValue(scroller.value - scroller.lineStep, true);
                        break;
                    
                    case NSScrollerPart.IncrementLine:
                        scroller.SetValue(scroller.value + scroller.lineStep, true);
                        break;
                    
                    case NSScrollerPart.DecrementPage:
                        scroller.SetValue(scroller.value - scroller.pageStep, true);
                        break;
                    
                    case NSScrollerPart.IncrementPage:
                        scroller.SetValue(scroller.value + scroller.pageStep, true);
                        break;
                    
                    case NSScrollerPart.Knob:
                        if (liveScrolling)
                        {
                            scroller.SetValueByKnob(DoubleValue);
                        }

                        break;
                    
                    case NSScrollerPart.KnobSlot:
                        scroller.SetValueByKnob(DoubleValue);
                        break;
                } 
            }
        }

        public Scrollbar(float x, float y) : this(x, y, DefaultWidth, DefaultHeight)
        { }

        public Scrollbar(float x, float y, float width, float height)
        {
            Handle = new NSScrollerEx(new CGRect(x, y, width, height), this)
            {
                DoubleValue = 0.0f,
                Enabled = true
            };

            lineStep = 1;
            minimum = 0;
            maximum = 100;
            PageStep = 10;
            enabled = true;
            value = 0;
            
            
        }

        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
        protected void OnValueChanged(bool invokedByUser)
        {
            ValueChanged?.Invoke(this, new ActionEventArgs(invokedByUser));
        }

        private void SetValueByKnob(double newValue)
        {
            var newIntValue = (int) Math.Min(Math.Max(((maximum - minimum) * newValue) + minimum, minimum), maximum);

            if (newIntValue != value)
            {
                value = newIntValue;
                OnValueChanged(true);
            } 
            
        }
        
        private void SetValue(int newValue, bool setByUserInterface)
        {
            {
                if (value == newValue)
                {
                    return;
                }
                
                value = 
                    (newValue < minimum) ? minimum : 
                    (newValue > maximum) ? maximum : newValue;

                if(maximum != 0)
                {
                    ((NSScroller) Handle).DoubleValue = minimum + (((value) * 1.0f) / (maximum - minimum));
                    ((NSScroller) Handle).SetNeedsDisplay();
                }
                
                OnValueChanged(setByUserInterface);
            }
        }

        public bool LiveScrolling
        {
            get => ((NSScrollerEx) Handle).liveScrolling;
            set => ((NSScrollerEx) Handle).liveScrolling = value;
        }
        
        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
        public int Value
        {
            get => value;
            set => SetValue(value, false);
        }

        public int Minimum
        {
            get => minimum;
            set
            {
                if(Value < value)
                {
                    Value = value;
                }

                minimum = value;

                ((NSScroller) Handle).Enabled = enabled && maximum > minimum;
                ((NSScroller) Handle).SetNeedsDisplay();
            }
        }

        public int Maximum
        {
            get => maximum;
            set
            {
                if(Value > value)
                {
                    Value = value;
                }

                maximum = value;

                ((NSScroller) Handle).Enabled = enabled && maximum > minimum;
                ((NSScroller) Handle).SetNeedsDisplay();
            }
        }

        public bool Enabled
        {
            get => enabled;
            set
            {
                if (value != enabled)
                {
                    enabled = value;
                    
                    ((NSScroller) Handle).Enabled = enabled && maximum > minimum;
                    ((NSScroller) Handle).SetNeedsDisplay();
                }
                
            }
        }

        public int LineStep
        {
            get => lineStep;
            set => lineStep = value;
        }

        [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
        public int PageStep
        {
            get => pageStep;
            set
            {
                if (value < 1)
                {
                    value = 1;
                }

                if (value != pageStep)
                {
                    pageStep = value;
                    ((NSScroller) Handle).KnobProportion = (pageStep * 1.0f) / (maximum - minimum + pageStep);
                }
            }
        }
        
        public override void Dispose()
        {
            ValueChanged = null;

            base.Dispose();
        }
    }
}