using System;
using AppKit;
using CoreGraphics;

namespace Einhugur.Forms
{
    public class Screen
    {
        private readonly NSScreen screen;
        
        public static int Count => NSScreen.Screens.Length;

        public static Screen GetScreen(int index)
        {
            if (index < 0 || index > NSScreen.Screens.Length - 1)
            {
                throw new ArgumentException("Invalid value for index parameter");
            }
            
            return new Screen(NSScreen.Screens[index]);
        }

        public static Screen MainScreen => new Screen(NSScreen.MainScreen);

        private Screen(NSScreen screen)
        {
            this.screen = screen;
        }

        public double ScaleFactor => screen.BackingScaleFactor;

        public int Width => (int)screen.Frame.Width;
        public int Height => (int)screen.Frame.Height;
        public int Left => (int)screen.Frame.GetMinX();
        public int Top => (int)(screen.Frame.Height - (screen.Frame.GetMinY() + screen.Frame.Height));
        
        public int AvailableWidth => (int)screen.VisibleFrame.Width;
        public int AvailableHeight => (int)screen.VisibleFrame.Height;
        public int AvailableLeft => (int)screen.VisibleFrame.GetMinX();
        public int AvailableTop => (int)(screen.VisibleFrame.Height - (screen.VisibleFrame.GetMinY() + screen.VisibleFrame.Height));
    }
}