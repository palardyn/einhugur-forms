﻿using System;
using System.Diagnostics.CodeAnalysis;
using AppKit;
using CoreGraphics;
using Einhugur.Arguments;
using Einhugur.Menus;
using Foundation;
using JavaScriptCore;


namespace Einhugur.Forms
{
	public class Slider : Control
    {
        public const int DefaultHeight = 16;
        public const int DefaultWidth = 200;
              
        public event EventHandler<ActionEventArgs> ValueChanged;

        private sealed class NSSliderEx : NSSlider
        {
            public bool liveScrolling;
            private readonly WeakReference<Slider> owner;

            public NSSliderEx(CGRect frameRect, Slider owner)
                : base(frameRect)
            {
                Target = this;
                Action = new ObjCRuntime.Selector("sliderAction:");

                this.owner = new WeakReference<Slider>(owner);

                liveScrolling = true;
            }

            [Export("sliderAction:")]
            public void doAction(NSObject sender)
            {
                if (owner.TryGetTarget(out Slider slider))
                {
                    slider.OnAction(false);
                }
            }
        }


        public Slider(float x, float y) : this(x, y, DefaultWidth, DefaultHeight)
        { }

        public Slider(float x, float y, float width, float height)
        {
            Handle = new NSSliderEx(new CGRect(x, y, width, height), this)
            {
                DoubleValue = 0.0f,
                Enabled = true,
                SliderType = NSSliderType.Linear
            };                       
        }

        protected virtual void OnAction(bool invokedByUser)
        {
            ValueChanged?.Invoke(this, new ActionEventArgs(invokedByUser));
        }

        public double Value
        {
            get => ((NSSlider)Handle).DoubleValue;
            set {
                ((NSSlider)Handle).DoubleValue = value;
                OnAction(true);
            }
        }

        public double Minimum
        {
            get => ((NSSlider)Handle).MinValue;
            set {
                ((NSSlider)Handle).MinValue = value;
                ((NSSlider)Handle).SetNeedsDisplay();
            }
        }

        public double Maximum
        {
            get => ((NSSlider)Handle).MaxValue;
            set {
                ((NSSlider)Handle).MaxValue = value;
                ((NSSlider)Handle).SetNeedsDisplay();
            }
        }

        public bool Enabled
        {
            get => ((NSSlider)Handle).Enabled;
            set => ((NSSlider)Handle).Enabled = value;
        }
    }
}

