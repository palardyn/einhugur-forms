﻿using System;
using System.Collections.Generic;
using AppKit;

namespace Einhugur.Forms
{
    public class GroupBox : ContainerControl
    {
        public const int DefaultHeight = 100;
        public const int DefaultWidth = 100;

        public GroupBox(float x, float y, float width, float height, Func<IEnumerable<Control>> setupControls = null)
        {
            Handle = new NSBox(new CoreGraphics.CGRect(x, y, width, height));

            ((NSBox)Handle).ContentView = new ContentViewEx();

            if (setupControls != null)
            {
                SetupControls(setupControls);
            }
        }

        public string Caption
        {
            get => ((NSBox)Handle).Title;
            set => ((NSBox)Handle).Title = value;
        }
    }
}

