﻿
using AppKit;
using Einhugur.Forms;

namespace Einhugur.Dialogs
{
    public enum MessageBoxStyle : ulong
    {
        Informational = NSAlertStyle.Informational,
        Warning = NSAlertStyle.Warning,
        Critical = NSAlertStyle.Critical
    };
    
    public class MessageBox
    {
        private readonly NSAlert alert;
        
        public MessageBox()
        {
            alert = new NSAlert()
            {
                AlertStyle = NSAlertStyle.Informational
            };
        }

        public string Message
        {
            get => alert.MessageText;
            set => alert.MessageText = value;
        }
        
        public string Information
        {
            get => alert.InformativeText;
            set => alert.InformativeText = value;
        }

        public MessageBoxStyle Style
        {
            get => (MessageBoxStyle)alert.AlertStyle;
            set => alert.AlertStyle = (NSAlertStyle)value;
        }
        
        public bool ShowsSuppressionButton
        {
            get => alert.ShowsSuppressionButton;
            set => alert.ShowsSuppressionButton = value;
        }

        public Button SuppressionButton => new Button(alert.SuppressionButton);

        public Button AddButton(string title) => new Button(alert.AddButton(title));

        public int ShowModal() => (int)alert.RunModal() - 1000;

        public int ShowModal(Window window) => (int)alert.RunSheetModal(window.Handle) - 1000;


        public static void Show(string message, string title = "")
        {
            var alert = new NSAlert()
            {
                AlertStyle = NSAlertStyle.Informational,
                InformativeText = message,
                MessageText = title,
            };
            alert.RunModal();
        }
    }
}

