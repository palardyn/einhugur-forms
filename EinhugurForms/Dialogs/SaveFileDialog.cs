﻿
using System.Diagnostics.CodeAnalysis;
using System.Net;
using AppKit;

namespace Einhugur.Dialogs
{
    [SuppressMessage("ReSharper", "MemberCanBeProtected.Global")]
    public class SaveFileDialog
    {
        protected NSSavePanel panel;

        public static FileSystemItem Show(string[] fileTypes)
        {
            var dlg = new SaveFileDialog();

            if (fileTypes == null)
            {
                return null;
            }

            dlg.AllowedFileTypes = fileTypes;

            if (dlg.Show())
            {
                return dlg.Url;
            }

            return null;
        }

        public SaveFileDialog()
        {
            panel = new NSSavePanel();
            panel.ExtensionHidden = false;
            panel.CanCreateDirectories = true;
        }

        public string[] AllowedFileTypes
        {
            get => panel.AllowedFileTypes;
            set => panel.AllowedFileTypes = value;
        }

        public string Title
        {
            get => panel.Title;
            set => panel.Title = value; 
        }

        public string Message
        {
            get => panel.Message;
            set => panel.Message = value;
        }

        public string Prompt
        {
            get => panel.Prompt;
            set => panel.Prompt = value; 
        }

        public bool Show()
        {
            return panel.RunModal() != 0;
        }

        public FileSystemItem Url => new FileSystemItem(panel.Url.Path);
    }
}

