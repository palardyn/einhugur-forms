﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DocGen
{
    class ClassInterfaceExtractor
    {
        public class FrameworkParameter
        {
            public string Name { get; set; }
            public string Type { get; set; }
        }

        public class FrameworkProperty
        {
            public string Name { get; set; }
            public string Type { get; set; }

            public string GetterScope { get; set; }

            public string SetterScope { get; set; }

            public bool HasGetter { get; set; }

            public bool HasSetter { get; set; }

            public bool IsStatic { get; set; }
        }

        public class FrameworkMethod
        {
            public string ReturnType { get; set; }
            public string Name { get; set; }

            public string Scope { get; set; }

            public FrameworkParameter[] Parameters { get; set; }

            public bool IsStatic { get; set; }
        }

        public class FrameworkClass
        {
            public string Name { get; set; }

            public string ParentName { get; set; }

            public bool IsControl { get; set; }

            public bool IsAbstract { get; set; }

            public string[] ImplementedInterfaces { get; set; }

            

            public FrameworkMethod[] Events { get; set; }

            public FrameworkProperty[] Properties { get; set; }

            public FrameworkMethod[] Constructors { get; set; }

            public FrameworkMethod[] Methods { get; set; }

            
        }





        static Dictionary<string, string> TypePreprocessor = new Dictionary<string, string>()
        {
            { "Int32", "int" },
            { "String", "string" },
            { "Void", "void" },
            { "Double", "double" },
            { "Single", "float" },
            { "Boolean", "bool" }
        };

        static string PreprocessType(Type type)
        {
            if(type.IsGenericType)
            {
                var parts = type.GetGenericArguments();

                var res = parts.Select(x => PreprocessType(x)).ToArray();

                var result = type.Name.Split('`').First() +
                    "<" + string.Join(", ", res) + ">";

                return result;
            }
            

            if (TypePreprocessor.ContainsKey(type.Name))
            {
                return TypePreprocessor[type.Name];
            }

            return type.Name;
        }

        static bool IsProperty(string methodName)
        {
            return methodName.StartsWith("get_") || methodName.StartsWith("set_");
        }

        private FrameworkParameter[] GetFrameworkParameters(System.Reflection.ParameterInfo[] prms, bool writeToConsole)
        {
            var parameters = new List<FrameworkParameter>();

            foreach (var prm in prms)
            {
                var frameworkParameter = new FrameworkParameter()
                {
                    Name = prm.Name,
                    Type = PreprocessType(prm.ParameterType)
                };


                parameters.Add(frameworkParameter);

                if (writeToConsole)
                {
                    Console.WriteLine($"- {frameworkParameter.Type} {prm.Name} {prm.DefaultValue}");
                }


            }

            return parameters.ToArray();
        }

        private void DisplayConstructorInfo(System.Reflection.ConstructorInfo[] myArrayMethodInfo, FrameworkClass c, bool writeToConsole)
        {
            var constructors = new List<FrameworkMethod>();

            for (int i = 0; i < myArrayMethodInfo.Length; i++)
            {
                System.Reflection.ConstructorInfo myMethodInfo = myArrayMethodInfo[i];


                var method = new FrameworkMethod();
                method.Name = myMethodInfo.Name;
                method.ReturnType = "void";
                method.Scope = myMethodInfo.IsPublic ? "public" : "protected";
                method.IsStatic = false;

                if (writeToConsole)
                {
                    Console.WriteLine($"\n{method.ReturnType} {method.Name}");
                }

                //var parameters = new List<FrameworkParameter>();

                method.Parameters = GetFrameworkParameters(myMethodInfo.GetParameters(), writeToConsole);

                if (!myMethodInfo.IsPrivate)
                {
                    constructors.Add(method);
                }
            }

            c.Constructors = constructors.ToArray();
        }

        private void DisplayEventInfo(System.Reflection.EventInfo[] myArrayMethodInfo, FrameworkClass c, bool writeToConsole)
        {
            var events = new List<FrameworkMethod>();

            for (int i = 0; i < myArrayMethodInfo.Length; i++)
            {
                System.Reflection.EventInfo myMethodInfo = myArrayMethodInfo[i];


                var method = new FrameworkMethod();
                method.Name = myMethodInfo.Name;
                method.ReturnType = "void";
                method.Scope = "public";
                method.IsStatic = false;

                if (writeToConsole)
                {
                    Console.WriteLine($"\n{method.ReturnType} event {method.Name}");
                }

                //var parameters = new List<FrameworkParameter>();

                var t = myMethodInfo.EventHandlerType;

                System.Reflection.MethodInfo[] methods = t.GetMethods(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);

                var invoker = methods.Where(x => x.Name == "Invoke").First();

                method.Parameters = GetFrameworkParameters(invoker.GetParameters(), writeToConsole);

                events.Add(method);
            }

            c.Events = events.ToArray();
        }

        private void DisplayMethodInfo(System.Reflection.MethodInfo[] myArrayMethodInfo, FrameworkClass c, bool writeToConsole)
        {
            // Display information for all methods.
            var methods = new List<FrameworkMethod>();
            var properties = new Dictionary<string, FrameworkProperty>();

            for (int i = 0; i < myArrayMethodInfo.Length; i++)
            {
                System.Reflection.MethodInfo myMethodInfo = myArrayMethodInfo[i];

                if (myMethodInfo.MemberType == System.Reflection.MemberTypes.Method)
                {
                    var method = new FrameworkMethod();
                    method.Name = myMethodInfo.Name;
                    method.ReturnType = PreprocessType(myMethodInfo.ReturnType);
                    method.IsStatic = myMethodInfo.IsStatic;

                    if(method.Name.StartsWith("add_") || method.Name.StartsWith("remove_"))
                    {
                        continue;
                    }
                   

                    if (IsProperty(method.Name))
                    {
                        FrameworkProperty prop;

                        string name = method.Name.Substring(4, method.Name.Length - 4);


                        if (properties.ContainsKey(name))
                        {
                            prop = properties[name];

                        }
                        else
                        {
                            prop = new FrameworkProperty();
                        }

                        if (!myMethodInfo.IsPrivate)
                        {
                            prop.Name = name;


                            if (method.Name.StartsWith("get_"))
                            {
                                prop.Type = method.ReturnType;
                                prop.HasGetter = true;
                                prop.GetterScope = myMethodInfo.IsPublic ? "public" : "protected";
                            }
                            else
                            {
                                prop.Type = PreprocessType(myMethodInfo.GetParameters()[0].ParameterType);
                                prop.HasSetter = true;
                                prop.SetterScope = myMethodInfo.IsPublic ? "public" : "protected";
                            }
                            prop.IsStatic = myMethodInfo.IsStatic;


                            properties[name] = prop;
                        }


                    }
                    else
                    {
                        method.Scope = myMethodInfo.IsPublic ? "public" : "protected";


                        if (writeToConsole)
                        {
                            Console.WriteLine($"\n{method.ReturnType} {myMethodInfo.Name}");
                        }

                        method.Parameters = GetFrameworkParameters(myMethodInfo.GetParameters(), writeToConsole);


                        if (!myMethodInfo.IsPrivate)
                        {
                            methods.Add(method);
                        }
                    }
                }
            }

            c.Methods = methods.ToArray();
            c.Properties = properties.Values.ToArray();
        }

        private string ResolveGenerics(Type type)
        {
            if (type.IsGenericType)
            {
                var parts = type.GetGenericArguments();

                var res = parts.Select(x => PreprocessType(x)).ToArray();

                var result = type.Name.Split('`').First() +
                    "<" + string.Join(", ", res) + ">";

                return result;
            }

            return type.Name;
        }

        public void ProcessClasses(Type[] classTypes, List<FrameworkClass> classes, bool writeToConsole)
        {
            foreach (var classType in classTypes)
            {
                var c = new FrameworkClass();
                c.Name = classType.Name;
                c.IsAbstract = classType.IsAbstract;
                c.ParentName = classType.BaseType?.Name;
                c.IsControl = classType.IsSubclassOf(typeof(Einhugur.Forms.Control)) || classType == typeof(Einhugur.Forms.Control);

                // Get the public methods.
                System.Reflection.MethodInfo[] myArrayMethodInfo = classType.GetMethods(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.DeclaredOnly);

                // Display all the methods.
                DisplayMethodInfo(myArrayMethodInfo, c, writeToConsole);
                var constructors = classType.GetConstructors(System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
                DisplayConstructorInfo(constructors, c, writeToConsole);

                var events = classType.GetEvents(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);
                DisplayEventInfo(events, c, writeToConsole);

                c.ImplementedInterfaces = classType.GetInterfaces().Select(x => ResolveGenerics(x)).ToArray();

                classes.Add(c);
            }
        }
    }
}


