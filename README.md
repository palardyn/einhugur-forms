# Einhugur Forms #

GUI framework for .NET on Mac, using code style like Windows forms for mortals that do not like the Xaml or Storyboard way of thinking.

### Requirements: ###

* Visual Studio 2022 Preview version or later or Rider from Jetbrains.

We have a tool to help you create new hello world projects to use as base for your own projects:

[Einhugur Project Generator](https://www.einhugur.com/Downloads/EinhugurProjectGeneratorx86_64.dmg)

### Documentation: ###

Note to run the test project you may need to right click EinhugurFormsTest inside the solution and click "Set as startup project"

**Current documentation can be downloaded from:**

(https://einhugur.net/Downloads/EinhugurForms/Documentation.zip)

**Note the documentation is Auto generated, and currently has Xojo dialect until we have added C# dialect in our documentation generator**
